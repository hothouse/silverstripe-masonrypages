$(function () {
	var $Masonry = $('.masonry');
	$Masonry.each(function () {
		var $MasonryGrid = $(this).find('.masonry-grid');
		if ($MasonryGrid.length) {
			var
					wall = new freewall($MasonryGrid),
					masonryBrickSelector = '.tag-all',
					$MasonryBricks = $(masonryBrickSelector),
					masonryID = $(this).attr('id'),
					$MasonryGridSizer = $(this).find('.masonry-grid-sizer'),
					$MasonryGridNav = $(this).find('.masonry-grid-nav li a'),
					margin = 2 * Math.abs(parseInt($MasonryGrid.css('margin-left').replace('px', '')));

			// set zero margins and paddings
			var margins = {'padding-left': 0, 'padding-right': 0, 'margin-left': 0, 'margin-right': 0};
			$MasonryGrid.css(margins).css({'transition': 'height 0.5s', overflow: 'hidden'});
			$MasonryBricks.css(margins);
			$MasonryGridSizer.css(margins);

			// settings
			var masonryOptions = $.extend({
				selector: masonryBrickSelector,
				animate: true,
				cellW: function (width) {
					return parseInt($MasonryGridSizer.width());
				},
				cellH: 'auto',
				delay: 50,
				gutterX: margin,
				gutterY: margin,
				onResize: function () {
					this.fitWidth();
				}
			}, masonryOptions || {});

			// on tab show
			$('a[href*="' + masonryID + '"][data-toggle="tab"]').on('shown.bs.tab', function (e) {
				wall.reset(masonryOptions);
				wall.fitWidth();
			});

			$(window).load(function () {
				if($MasonryGrid.is(':visible')) {
					wall.reset(masonryOptions);
					wall.fitWidth();
				}
				$MasonryGridNav.each(function () {
					var $this = $(this);
					$this.click(function () {
						$parent = $(this).parent();
						$parent.siblings().removeClass('active').end().addClass('active');
						var tagID = $parent.attr('id');
						tagID.indexOf('all') > -1 ? wall.unFilter() : wall.filter('.' + tagID);
						return false;
					});
					var tag = $this.attr('href').substring($this.attr('href').indexOf('#'));
					$('a[href$="' + tag + '"]').not($this).click(function () {
						$this.trigger('click');
					});
				});

				if (location.hash) {
					$('a[href$="' + location.hash + '"]').trigger('click');
				}
			});
		}
	});
});
