<?php

class MasonryArticlePage extends Page {

	private static $db = array(
		'IntroText' => 'Text'
	);

	private static $has_one = array(
		'IntroImage' => 'Image'
	);

	private static $many_many = array(
		'PageTags' => 'TaxonomyTerm'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->addFieldToTab("Root.Intro", TextAreaField::create('IntroText'));
		$Folder = $this->ParentID ? $this->Parent()->URLSegment : $this->URLSegment;
		$fields->addFieldToTab("Root.Intro", UploadField::create('IntroImage')->setFolderName($Folder));

		// tags
		$taxonomyMap = TaxonomyTerm::get();
		if($ParentTerm = $this->config()->get('taxonomy_parent_id')) {
			$taxonomyMap = $taxonomyMap->byID($ParentTerm)->Children();
		}
		$taxonomyMap = $taxonomyMap->map("ID", "Name")->toArray();
		asort($taxonomyMap);
		$taxonomy = ListboxField::create('PageTags', singleton('TaxonomyTerm')->i18n_plural_name())
			->setMultiple(true)
			->setSource($taxonomyMap)
			->setAttribute('data-placeholder', 'Tags');
		$fields->insertBefore($taxonomy, 'Content');

		return $fields;
	}

}

class MasonryArticlePage_Controller extends Page_Controller {
}