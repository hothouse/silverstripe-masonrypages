<?php

class MasonryGridPage extends Page {

	public function ChildrenPageTags() {
		$Tags = TaxonomyTerm::get()
			->innerJoin('MasonryArticlePage_PageTags', '"TaxonomyTerm"."ID"="MasonryArticlePage_PageTags"."TaxonomyTermID"')
			->innerJoin('SiteTree', "\"SiteTree\".\"ID\"=\"MasonryArticlePage_PageTags\".\"MasonryArticlePageID\" AND \"SiteTree\".\"ParentID\"='$this->ID'")
			->sort('Name');

		$List = ArrayList::create();
		foreach($Tags as $Tag) {
			$List->push(
				ArrayData::create(
					array(
						'Object' => $Tag,
						'ID' => $Tag->ID,
						'Title' => $Tag->Name,
						'MenuTitle' => $Tag->Name,
						'Hash' => Convert::raw2url($Tag->Name),
						'Link' => '#'.Convert::raw2url($Tag->Name)
					)
				)
			);
		}

		return $List;
	}

	public function ChildrenPageTagList() {
		Requirements::combine_files(
			'masonry.js',
			array(
				MODULE_PAGETAGS_PATH.'/javascript/freewall.js',
				MODULE_PAGETAGS_PATH.'/javascript/script.js'
			)
		);
		return $this->ChildrenPageTags();
	}
}

class MasonryGridPage_Controller extends Page_Controller {
}