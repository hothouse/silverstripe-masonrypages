<% cached 'masonrygrid', $ID, $CurrentMember.ID, $List('Page').max('LastEdited') %>
	<div class="row masonry-grid">
		<div class="masonry-grid-sizer col-xs-12 col-sm-6 col-md-1"></div>
		<% loop $Children %>
			<div class="col-xs-12 col-sm-6 col-md-4 visible tag-all<% loop $PageTags %> tag-{$ID}<% end_loop %>">
				<% if $IntroImage %>
					<a href="$Link">
						<img src="$IntroImage.CroppedImage(380,245).Link" class="img-responsive" alt=""/>
					</a>
				<% end_if %>
				<h3>
					<a href="$Link">
						$Title
					</a>
				</h3>
				<p>$Content.LimitWordCount(20)</p>
			</div>
		<% end_loop %>
	</div>
<% end_cached %>